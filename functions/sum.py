'''
The sum module currently consists of the sum function.
'''

import numbers
import decimal

def sum(*argv):
    '''
    Calculates the sum of two or more numeric values (decimals included)

        Parameters:
            argv: two or more numeric values
        Returns:
            The sum of all values contains in *argv
    '''
    if len(argv) < 2:
        raise TypeError('Expecting at least two numeric arguments')
    res = argv[0]
    for arg in argv[1:]:
        if not isinstance(arg, (decimal.Decimal, numbers.Number)):
            raise ValueError('Not a numeric value')
        res += arg
    return res
