
from unittest import TestCase
from numbers import Number
from decimal import Decimal

from functions.sub import sub

class SubNotEnoughArgs(TestCase):

    def test_no_args(self):
        self.assertRaises(Exception, sub)

    def test_one_nan_arg(self):
        self.assertRaises(Exception, sub, 'x')

    def test_one_numeric_arg(self):
        self.assertRaises(Exception, sub, 1)
        self.assertRaises(Exception, sub, Decimal('0.1'))

class SubNaNArgs(TestCase):

    def test_one_nan_arg(self):
        self.assertRaises(Exception, sub, 'x')

    def test_two_nan_args(self):
        self.assertRaises(ValueError, sub, 'x', 'y')

    def test_multipe_nan_args(self):
        def args_gen():
            for arg in args:
                yield arg

        args = [str(arg) for arg in list(range(-100, 100))]
        self.assertRaises(ValueError, sub, *args)

class SubNumericArgs(TestCase):

    def test_one_numeric_arg(self):
        self.assertRaises(Exception, sub, 1)
        self.assertRaises(Exception, sub, Decimal('0.1'))

    def test_two_numeric_args(self):
        self.assertEqual(sub(1,1), 0)
        self.assertEqual(sub(Decimal('0.1'), Decimal('0.1')), Decimal('0.0'))

    def test_multipe_numeric_args(self):
        def args_gen():
            for arg in args:
                yield arg

        args = [Decimal(str(arg)) for arg in list(range(1, 3))]
        self.assertEqual(sub(*args), Decimal('-1'))