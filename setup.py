#!/usr/bin/env python

from distutils.core import setup

setup(name='functions',
      version='0.1',
      description='Functions',
      author='Aleksandar Atanasov',
      author_email='aleksandar.vl.atanasov@gmail.com',
      url='https://functions.org',
      packages=['functions']
     )